using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Recursos_Humanos
{
    public partial class Inicio : Form
    {
        Conexion con = null;
        public Inicio()
        {
            InitializeComponent();
            getContrasena.PasswordChar = '*';
            conectar();
    }
        // Inicio mover ventana
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void titulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        // Fin mover ventana
        public void conectar()
        {
            con = new Conexion();
            carga.Text = "Conexión establecida con la base de datos";
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void BotonConectar_Click(object sender, EventArgs e)
        {
            con.credenciales(getUsuario.Text, getContrasena.Text);
            if (con.cerrarCredenciales == true)
            {
                this.Hide();
            }
        }
        private void pruebaConexiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            con.pruebaConexion();
        }
    }
}
