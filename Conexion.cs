using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace Recursos_Humanos
{
    class Conexion
    {
        private SqlConnection con = null;
        //metodo credenciales
        private string miClave, miUsuario, miPrivilegio = null;
        //fin metodo
        public bool cerrarCredenciales = false;
        public DataTable dt = new DataTable();

        //metodo cargarMesActual
        public string run;
        public string nombre;
        public string apellido;
        public string sueldo;
        public string gratificacion;
        // fin metodo
        public DataTable tabla = new DataTable();

        public Conexion()
        {
            con = new SqlConnection("Data Source='ATAPIA\\CONEXION';Initial Catalog=rh;Pwd=dasrwtbo;User ID=sa");
        }

        public void pruebaConexion()
        {
            con = new SqlConnection("Data Source='ATAPIA\\CONEXION';Initial Catalog=rh;Pwd=dasrwtbo;User ID=sa");
            con.Open();
            con.Close();
            MessageBox.Show("Conexión exitosa");
        }

        public void credenciales(string usuario, string contrasena)
        {
            try
            {
                try
                {
                    Convert.ToInt32(usuario.Substring(0, usuario.Length - 1));
                    con.Open();
                    SqlCommand comando = new SqlCommand(null, con);
                    comando.CommandText = "SELECT run, contrasena, privilegios FROM usuario where run=@u";
                    SqlParameter parametro_uno = new SqlParameter("@u", SqlDbType.VarChar, 9);
                    parametro_uno.Value = usuario;
                    comando.Parameters.Add(parametro_uno);
                    comando.Prepare();
                    SqlDataReader r = comando.ExecuteReader();
                    if (r.Read())
                    {
                        miUsuario = r["run"].ToString();
                        miClave = r["contrasena"].ToString();
                        miPrivilegio = r["privilegios"].ToString();

                    }
                    if (miUsuario == usuario)
                    {
                        if (miClave == contrasena)
                        {
                            if (miPrivilegio == "1")
                            {
                                MessageBox.Show("Registrado como administrador");
                                Administrador uno = new Administrador();
                                uno.Show();
                                cerrarCredenciales = true;
                            }
                            else
                            {
                                MessageBox.Show("Registrado como Usuario");
                                Usuario dos = new Usuario();
                                dos.recibeDatos(miUsuario);
                                dos.Show();
                                cerrarCredenciales = true;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Clave incorrecta");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Usuario incorrecto");
                    }
                }
                catch
                {
                    if (usuario == "" || contrasena == "")
                    {
                        MessageBox.Show("Debe llenar los campos");
                    }
                    else
                    {
                        MessageBox.Show("El usuario debe ser tipo número sin guión");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Favor reinicar conexión");
            }
            con.Close();
        }


        public void cargarActual(string entrada)
        {
            try
            {
                con.Open();
                SqlCommand comando = new SqlCommand(null, con);
                comando.CommandText = "SELECT * FROM usuario where nombre=@u";
                SqlParameter parametro_uno = new SqlParameter("@u", SqlDbType.VarChar, 9);
                parametro_uno.Value = entrada;
                comando.Parameters.Add(parametro_uno);
                comando.Prepare();
                SqlDataReader r = comando.ExecuteReader();
                if (r.Read())
                {
                    run = r["run"].ToString();
                    nombre = r["nombre"].ToString();
                    apellido = r["apellido"].ToString();
                    sueldo = r["sueldo_bruto"].ToString();
                }
            }
            catch
            {
                MessageBox.Show("Excepción");
            }
            con.Close();
        }
        public void acceso(string entrada)
        {
            try
            {
                con.Open();
                SqlCommand comando = new SqlCommand(null, con);
                comando.CommandText = "SELECT privilegios, acceso FROM usuario where acceso=@u";
                SqlParameter parametro_uno = new SqlParameter("@u", SqlDbType.VarChar, 9);
                parametro_uno.Value = entrada;
                comando.Parameters.Add(parametro_uno);
                comando.Prepare();
                SqlDataReader r = comando.ExecuteReader();
                cerrarCredenciales = false;
                if (r.Read())
                {
                    if (entrada == r["acceso"].ToString() && r["privilegios"].ToString() == "1")
                    {
                        PrivilegioAutorizado aceptado = new PrivilegioAutorizado();
                        aceptado.Show();
                        cerrarCredenciales = true;
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error de registro al sistema");
            }
            con.Close();
        }
        public void actualizar(string Anombre, string Aapellido, int sueldo, string seleccion)
        {
            try
            {
                MessageBox.Show(Anombre.Length.ToString());
                if (Anombre.Length < 45 && apellido.Length < 45)
                {
                    con.Open();
                    SqlCommand comando = new SqlCommand(null, con);
                    comando.CommandText = "UPDATE Usuario SET nombre=@nombre, apellido=@apellido, gratificacion_legal=@grati WHERE nombre=@seleccion";
                    double grati = (sueldo * 25) / 100;
                    comando.Parameters.AddWithValue("@nombre", Anombre);
                    comando.Parameters.AddWithValue("@apellido", Aapellido);
                    comando.Parameters.AddWithValue("@grati", grati);
                    comando.Parameters.AddWithValue("@seleccion", seleccion);
                    comando.ExecuteNonQuery();
                    MessageBox.Show("Datos actualizados en base datos");
                }
                else
                {
                    MessageBox.Show("El largo de los datos no están permitidos");
                }
            }
            catch
            {
                MessageBox.Show("Favor reiniciar conexión");
            }
            con.Close();
        }

        public void actualizarSuper(string nombre, string apellido, string run, string sueldo, string seleccion)
        {
            try
            {
                con.Open();
                SqlCommand comando = new SqlCommand(null, con);
                comando.CommandText = "UPDATE Usuario SET nombre=@nombre, apellido=@apellido, run=@run, sueldo_bruto=@sueldo WHERE run=@seleccion";
                comando.Parameters.AddWithValue("@nombre", nombre);
                comando.Parameters.AddWithValue("@apellido", apellido);
                comando.Parameters.AddWithValue("@run", run);
                comando.Parameters.AddWithValue("@sueldo", sueldo);
                comando.Parameters.AddWithValue("@seleccion", seleccion);
                comando.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Favor reiniciar conexión");
            }
            con.Close();
        }


        public void llenarCombobox()
        {
            try
            {
                con.Open();
                SqlCommand comando = new SqlCommand(null, con);
                comando.CommandText = "SELECT nombre FROM usuario order by nombre";
                SqlDataAdapter lista = new SqlDataAdapter(comando);
                dt.Clear();
                lista.Fill(dt);
            }
            catch
            {
                MessageBox.Show("No se ha podido cargar lista, favor reiniciar conexión");
            }
            con.Close();
        }
        public void cargarUsaurio(string entrada)
        {
            try
            {
                con.Open();
                SqlCommand comando = new SqlCommand(null, con);
                comando.CommandText = "SELECT * FROM usuario where run=@u";
                SqlParameter parametro_uno = new SqlParameter("@u", SqlDbType.VarChar, 9);
                parametro_uno.Value = entrada;
                comando.Parameters.Add(parametro_uno);
                comando.Prepare();
                SqlDataReader r = comando.ExecuteReader();

                if (r.Read())
                {
                    run = r["run"].ToString();
                    nombre = r["nombre"].ToString();
                    apellido = r["apellido"].ToString();
                    sueldo = r["sueldo_bruto"].ToString();
                    gratificacion = r["gratificacion_legal"].ToString();
                }
            }
            catch
            {
                MessageBox.Show("Excepción carga usaurio");
            }
            con.Close();
        }

        public void vistaTabla()
        {
            try
            {
                con.Open();
                SqlCommand comando = new SqlCommand(null, con);
                comando.CommandText = "SELECT * from Usuario";
                SqlDataAdapter vista = new SqlDataAdapter(comando);
                tabla.Clear();
                vista.Fill(tabla);
            }
            catch
            {
                MessageBox.Show("Excepción");
            }
            con.Close();
        }

        public void calculoGratificacion(int sueldo, string seleccion)
        {
            try
            {
                con.Open();
                SqlCommand comando = new SqlCommand(null, con);
                comando.CommandText = "UPDATE Usuario SET gratificacion_legal=@gratif WHERE run=@seleccion";
                double grati = (sueldo * 25) / 100;
                comando.Parameters.AddWithValue("@gratif", grati);
                comando.Parameters.AddWithValue("@seleccion", seleccion);
                comando.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("No se ha podido realizar el cálculo");
            }
            con.Close();
        }
    }
}