using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recursos_Humanos
{
    public partial class Acceso : Form
    {
        Conexion ini = new Conexion();
        public Acceso()
        {
            InitializeComponent();
            cajaValidar.PasswordChar = '*';
        }

        private void Acceso_Load(object sender, EventArgs e)
        {

        }

        private void botonValidar_Click(object sender, EventArgs e)
        {
            ini.acceso(cajaValidar.Text);
            if (ini.cerrarCredenciales == true)
            {
                this.Dispose();
            }
        }
    }
}