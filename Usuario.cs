using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recursos_Humanos
{
    public partial class Usuario : Form
    {
        private string _usuario;
        public Usuario()
        {
            InitializeComponent();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
            Inicio ini = new Inicio();
            ini.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        public void recibeDatos(string a)
        {
            _usuario = a;
        }

        private void botonCargarU_Click(object sender, EventArgs e)
        {
            Conexion ver = new Conexion();
            ver.cargarUsaurio(_usuario);
            cajaNombreU.Text = ver.nombre;
            cajaApellidoU.Text = ver.apellido;
            cajaRunU.Text = ver.run;
            cajaSueldoU.Text = ver.sueldo;
            MessageBox.Show("Estimado " + cajaNombreU.Text + " los datos cargados son los más actualizados");
        }

        private void relojU_Tick(object sender, EventArgs e)
        {
            tiempo.Text = DateTime.Now.ToString();
        }

        private void Usuario_Load(object sender, EventArgs e)
        {
            relojU.Enabled = true;
        }
    }
}
