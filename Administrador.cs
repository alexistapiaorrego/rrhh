using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recursos_Humanos
{
    public partial class Administrador : Form
    {
        Conexion ver = new Conexion();
        public Administrador()
        {
            InitializeComponent();
            listarUsuarios();
        }

        private void botonCargar_Click(object sender, EventArgs e)
        {
            try
            {
                ver.cargarActual(cajaDesplegar.SelectedItem.ToString());
                cajaNombre.Text = ver.nombre;
                cajaApellido.Text = ver.apellido;
                cajaRun.Text = ver.run;
                cajaSueldo.Text = ver.sueldo;
                ver.calculoGratificacion(Convert.ToInt32(cajaSueldo.Text), cajaRun.Text);
                ver.cargarUsaurio(cajaRun.Text);
                cajaGratificacion.Text = ver.gratificacion;
                eventosAdmin.Items.Add(DateTime.Now.ToString() + " Registro cargado");
            }
            catch
            {
                MessageBox.Show("Selecciona un usuario");
            }
        }

        private void listarUsuarios()
        {
            ver.llenarCombobox();
            foreach (DataRow fila in ver.dt.Rows)
            {
                cajaDesplegar.Items.Add(Convert.ToString(fila["nombre"]));
            }
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
            Inicio ini = new Inicio();
            ini.Show();
        }

        private void botonActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                botonCargar_Click(null, null);
                ver.actualizar(cajaNombre.Text.ToString(), cajaApellido.Text.ToString(), Convert.ToInt32(cajaSueldo.Text), cajaDesplegar.SelectedItem.ToString());
                cajaDesplegar.Items.Clear();
                ver.dt.Clear();
                listarUsuarios();
                ver.cargarUsaurio(cajaRun.Text);
                cajaGratificacion.Text = ver.gratificacion;
                eventosAdmin.Items.Add(DateTime.Now.ToString() + " Se ha actualizado usuario " + cajaRun.Text.ToString());
            }
            catch
            {
                //MessageBox.Show("Debe seleccionar un nombre");
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void informaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void historiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Historia h = new Historia();
            h.Show();
        }

        // Reloj tiempo real
        private void Administrador_Load(object sender, EventArgs e)
        {
            tiempo.Enabled = true;
        }
        private void tiempo_Tick(object sender, EventArgs e)
        {
            reloj.Text = DateTime.Now.ToString();
        }
        // FIN Reloj tiempo real


        private void superUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Acceso abrir = new Acceso();
            abrir.Show();
            this.Dispose();
        }

        private void botonLiquidacion_Click(object sender, EventArgs e)
        {
            try
            {
                Liquidacion liq = new Liquidacion();
                liq.Show();
                MessageBox.Show(calendario.Value.ToString());
                eventosAdmin.Items.Add(DateTime.Now.ToString() + " Se ha replicado liquidación de usuario " + cajaRun.Text.ToString());
            }
            catch
            {
                MessageBox.Show("Excepción controlada");
            }
        }
    }
}
