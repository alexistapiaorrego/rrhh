using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recursos_Humanos
{
    public partial class PrivilegioAutorizado : Form
    {
        Conexion ver = new Conexion();
        private static string _nombre;
        private static string _apellido;
        private static string _run;
        private static string _sueldo;
        private static string _gratificacion;
        private static string _enlazaedicion;

        public PrivilegioAutorizado()
        {
            InitializeComponent();
        }


        private void botonCargar_Click(object sender, EventArgs e)
        {
            ver.vistaTabla();
            vistaGrillaA.DataSource = ver.tabla;
            listaEventos.Items.Add(DateTime.Now.ToString() + " Se ha cargado la lista de trabajadores");
        }

        private void vistaGrillaA_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                _nombre = vistaGrillaA.Rows[e.RowIndex].Cells["nombre"].Value.ToString();
                _apellido = vistaGrillaA.Rows[e.RowIndex].Cells["apellido"].Value.ToString();
                _run = vistaGrillaA.Rows[e.RowIndex].Cells["run"].Value.ToString();
                _sueldo = vistaGrillaA.Rows[e.RowIndex].Cells["sueldo_bruto"].Value.ToString();
                _gratificacion = vistaGrillaA.Rows[e.RowIndex].Cells["gratificacion_legal"].Value.ToString();
                _enlazaedicion = vistaGrillaA.Rows[e.RowIndex].Cells["run"].Value.ToString();
                cajaNombreA.Text = _nombre;
                cajaApellidoA.Text = _apellido;
                cajaRunA.Text = _run;
                cajaSueldoA.Text = _sueldo;
                cajaGratificacion.Text = _gratificacion;
                listaEventos.Items.Add(DateTime.Now.ToString() + " Activación de registro" + cajaRunA.Text + " con función de actualizar");
            }
            catch
            {
                MessageBox.Show("Sin carga de colaborador");
            }
        }

        private void listaEventos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void relojsSuper_Tick(object sender, EventArgs e)
        {
            tiempo.Text = DateTime.Now.ToString();
        }

        private void PrivilegioAutorizado_Load(object sender, EventArgs e)
        {
            relojsSuper.Enabled = true;
        }

        private void volverAAdministradorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Administrador ini = new Administrador();
            ini.Show();
            this.Dispose();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Inicio ini = new Inicio();
            ini.Show();
            this.Dispose();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void botonModificarA_Click(object sender, EventArgs e)
        {
            try
            {
                int valor = Convert.ToInt32(cajaSueldoA.Text);
                ver.calculoGratificacion(valor, cajaRunA.Text);
                ver.actualizarSuper(cajaNombreA.Text, cajaApellidoA.Text, cajaRunA.Text, cajaSueldoA.Text, _enlazaedicion);
                ver.vistaTabla();
                ver.cargarUsaurio(_enlazaedicion);
                cajaNombreA.Text = ver.nombre;
                cajaApellidoA.Text = ver.apellido;
                cajaRunA.Text = ver.run;
                cajaGratificacion.Text = ver.gratificacion;
                cajaSueldoA.Text = ver.sueldo;
                listaEventos.Items.Add(DateTime.Now.ToString() + " Registro " + cajaRunA.Text + " se ha modificado por el Super usaurio");
            }
            catch
            {
                MessageBox.Show("Debe cargar colaborador");
            }
        }

        private void botonEliminarA_Click(object sender, EventArgs e)
        {
            Desvinculacion ex = new Desvinculacion();
            ex.Show();
        }
    }
}
